from django.apps import AppConfig


class RecipesConfig(AppConfig): # Revisar em outro momento
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'recipes'
