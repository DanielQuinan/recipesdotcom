from django.urls import path
from . import views #importa views do app


urlpatterns = [
    path('', views.home), # home padrão do app
    path('recipes/<int:id>/', views.recipes) # <int:id> é passado para se basear na referência do id da receita
]
