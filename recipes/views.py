from django.shortcuts import render
from utils.recipes.factory import make_recipe

# Create your views here.
def home(request): #retorna html de pasta home
    return render(request, 'recipes/pages/home.html', context={
        'recipes': [make_recipe() for _ in range(10)], #gera as receitas a partir da função make_recipe do arquivo factory.py, nela os itens são montados pelo faker
    })

def recipes(request, id):
    return render(request, 'recipes/pages/recipe-view.html', context={
        'recipe': make_recipe(), #gera apenas uma receita aleatório a partir do faker
        'is_detail_page': True, #variável para identificar se é a página da receita em si
    })